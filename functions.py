def print_area_of_a_circle_with_radius(r):
    area_of_a_circle = 3.1415 * r**2
    print(area_of_a_circle)

if __name__ == '__main__':
    print_area_of_a_circle_with_radius(5)


# wywołanie funkcji to podanie jej nazwy i nawiasy - nawet jeżeli arg nie ma
# funkcja to mała część kodu, która ma swój konkretny cel, mogą przyjmować arg nie muszą, mogą
# zwracać wartości nie muszą

def print_hello():
    print('hello')

print_hello()


def upper_word(word: str) -> str:
    return word.upper()

big_dog = upper_word('dog')
print(big_dog)

def add_two_numbers(a, b):
    return a + b



c = add_two_numbers(3,5)
print(add_two_numbers(3,5))
print(add_two_numbers(False, True))
print(add_two_numbers(a=True, b=5))

# + przyjmuje różną rolę, w załeżności od rodzaju danych, mając
# number zsumuje

def compute_squer_of_number(number):
    return number ** 2

print(compute_squer_of_number(16))

def calulate_volume_cuboid(x,y,z):
    return x*y*z
def conversion_farenh_to_cel(temp):
    return (temp-30)/2

def print_hello():
    print("Hello")

def square_numbers(number):
    return number**2



if __name__ == '__main__':
    print (calulate_volume_cuboid(5,3,4))

    print(conversion_farenh_to_cel(90))

    print_hello()

    print(square_numbers(2.55))