import random
def print_ten_numbers():
    for i in range(10):
        print(i)

def print_every_second_number():
    for i in range (0,21,2):
        print (i)


def print_one_thirty():
    for n in range(1, 31):
        if n % 7 == 0:
            print(n)

def print_random_numbers(n):
    for i in range(n):
        print(random.randint(1,1000))

def give_back_list():
    numbers = []
    for i in range(10):
        numbers.append(random.randint(1000,5000))
    return numbers

def get_temp_in_C(temp_in_cel):
    return temp_in_cel * 9/5+32

def convert_cel_to_far(cel_list):
    far_list = []
    for tem in cel_list:
        tem_in_far = get_temp_in_C(far_list)
        tem_in_far.append(tem_in_far)
    return far_list




if __name__ == '__main__':
    print_ten_numbers()
    print_every_second_number()
    print_one_thirty()
    print_random_numbers(10)
    print(give_back_list())
    print(get_temp_in_C(10.3))
