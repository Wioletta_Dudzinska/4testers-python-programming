

a = 3

if a > 0: # bardzo popularny wyjątek, jeżeli coś nie jestem stringiem to rzuć wyj.
    print('a is positive')
else:
    print ('a is not positive')

def temperature(degrees, pressure):
    if degrees == 0 and pressure == 1013:
        return True
    else:
        return False

def calculate_test_score(points):
    if points >= 90:
        return 5
    elif points >= 75:
        return 4
    elif points >= 50:
        return 3
    else:
        return 2


def vaccination(kind,age):

    #if age == 1 and (kind == 'dog' or kind == 'cat'):
       # return True
    #elif kind == 'cat' and (age-1) % 3 == 0:
        #return True
    #elif kind == 'dog' and age % 2 != 0:
        #return True
    #else:
        #return False

    if age <= 0:
        raise ValueError('Age must be greater than one')
    elif kind not in ['dog', 'cat']:
        raise ValueError('The animal must be cat or dog')
    return (kind == 'dog' and (age-1)%2 == 0) or (kind == 'cat' and (age - 1)%3 == 0)




if __name__ == '__main__':
    print(temperature(1,2014))
    print(temperature(0,1013))
    print(temperature(1,1013))
    print(vaccination('dog',6))
    print(vaccination('cat',5))
    print(vaccination('lion',5))
