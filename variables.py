firstname = "Monika"
age = 40
pet_amount = 0
has_driving_license = True
friendship_years = 22

# print jest funkcją, która ma argumenty
# podając argumenty funkcji, nie używamy spacji

print("Name:", firstname, sep="\n")
print("Age:", age, sep="\n")
print("Pet amount:", pet_amount, sep="\n")
print("Has driving license:", has_driving_license, sep="\n")
print("Friendship_year:", friendship_years, sep="\n")

print("....................")

print(
    "Name:", firstname,
    "Age:", age,
    sep="\n"
)

name_surname = "Wiola Dudzinska"
email = "dudzinska.wioletta@gmail.com"
phone_number = "+48 000000000"

bio = "Name:" + name_surname + "\nEmail:" + email + "\nPhone:" + phone_number
print(bio)

bio_smarter = f"Name:{name_surname}\nEmail: {email}\nPhone: {phone_number}"
print(bio_smarter)